module.exports = {
    title: ' ',
    description: "Documentação para desenvolvimento",
     // Google Analytics tracking code
     // ga: "Analytics code",
     themeConfig: {
        search: true,
        searchMaxSuggestions: 10,
        logo: 'https://linkl.com.br/wp-content/uploads/2019/06/LinkL_Logo.png',
         nav: [
             { text: "Introdução", link: '/pageone' },
             { text: "Documentação", link: "/pagetwo" },
             { text: "APIs", link: "/pagethree" },
             { text: "Site Oficial", link:'https://linkl.com.br' }
         ],
         sidebar: [
            '/',
            '/pageone',
            '/pagetwo',
            '/pagethree'
        ]
     }
}