---
home: true
heroImage: 'https://linkl.com.br/wp-content/uploads/2019/06/LinkL_Logo.png'
actionText: 📝 Guide 
actionLink: /pageone
features:
- title: Ferramentas
  details: Aqui estao presentes todas as ferramentas necessarias para a criação da aplicação.
- title: Design & Patterns
  details: Padroes de criação podem ser encontrados neste documento.
- title: APIs
  details: Uma descrição sobre as APIs que são utilizadas dentro da aplicação e a forma como funcionam.
footer: Made with by Inundaweb
---
