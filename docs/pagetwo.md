---
title: Patterns
---

# Patterns

## Atomic design ⚛️

Funcionamento Atomic design, estrutura de pastas.

![An image](https://miro.medium.com/max/2000/0*w0R-1G5DsdqtAjxs.png)

### Funcionamento

Atomic Design , é crucial para a legibilidade, escalabilidade e flexibilidade do código. É uma metodologia para a elaboração de sistemas de projeto com cinco blocos de construção fundamentais, que, quando combinados, promovem consistência, modularidade e escalabilidade. 

### Desenvolvimento Atômico

Os cinco níveis distintos de design atômico - átomos> moléculas> organismos> modelos> páginas - mapeiam incrivelmente bem a arquitetura baseada em componentes do React.

![An image](https://user-images.githubusercontent.com/4838076/33235048-d083dca6-d217-11e7-9aea-9a5ef5ae6fe7.png)

> *Átomos:*
    Blocos de construção básicos da matéria, como um botão, entrada ou uma Labels de formulário. Eles não são úteis por conta própria.

> *Moléculas:*
    Agrupando átomos juntos, como combinar um botão, entrada e Labels de formulário para criar funcionalidade.

> *Organismos:*
    Combinação de moléculas para formar organismos que formam uma seção distinta de uma interface (por exemplo, barra de navegação)

> *Modelos:*
    Consistindo principalmente de grupos de organismos para formar uma página - onde os clientes podem ver um projeto final no lugar.

> *Páginas:*
    Um ecossistema que exibe diferentes renderizadores de modelo. Podemos criar vários ecossistemas em um único ambiente - o aplicativo.

## React ⚛️

![An image](https://github.com/grab/front-end-guide/raw/master/images/desk.png)

O Sistema foi construido baseado na biblioteca *React*, desenvolvida pelo Facebook, com o auxilio do *Redux*, arquitetura flux, para gerenciamento de estado. A arquitetura da aplicação é constituida em cinco pastas principais:

   - *MOLECULES*

   - *COMPONENTS*

   - *MODELS*

   - *PUBLIC*

   - *UTILS*